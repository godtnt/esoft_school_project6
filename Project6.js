class GameField {
    // state - поле игры
    state = [
        [null, null, null],
        [null, null, null],
        [null, null, null]
    ];
    // setMode - метод для понимания чей ход, xodX = true - ход Крестиков, xodX = false - Ноликов
    setMode = function (xod) {
        let xodX = null;

        if (xod % 2 == 0) {
            xodX = true;
        } else {
            xodX = false;
        };
        
        return xodX
    };
    // isOverGame - метод для понимания завершилась ли игра
    isOverGame = function () {
        let isOverGame = null;
        if ((winXCondition(this.state.flat()) == true) || (winOCondition(this.state.flat()) == true)) {
            isOverGame = true
        } else {
            isOverGame = false
        }
        return isOverGame
    };
    // getGameFieldStatus - метод для вывода игрового поля в консольку
    getGameFieldStatus = function () {
        console.log(`          `)
        console.log(`__  __  __`)
        for (let cell of this.state) {
            console.log(`${cell[0]} | ${cell[1]} | ${cell[2]}`)
            console.log(`__  __  __`)
        }
    };
    // fieldCellValue - метод для хода
    fieldCellValue = function (counterXod) {
        const column = prompt(`введите столбец:`)
        const line = prompt(`введите строку:`)
        if ((column < 4) || (line < 4)) {
            if ((this.state[line - 1][column - 1] === null) && (this.setMode(counterXod) == true)) {
                this.state[line - 1][column - 1] = 'x'
            } else if ((this.state[line - 1][column - 1] === null) && (this.setMode(counterXod) == false)) {
                this.state[line - 1][column - 1] = 'o'
            } else {
                alert(`указанное поле занято`)
                this.fieldCellValue()
            }
        } else {
            alert(`введены не коректные данные`)
        }
    }
}

// Функции winXCondition и winOCondition функции для проверки победы Крестиков и Ноликов соответственно
function winXCondition (arrey) {
    const firstCell = arrey[0]; const secondCell = arrey[1]; const thirdCell = arrey[2]; const fourthCell = arrey[3]; const fifthCell = arrey[4]; const sixthCell = arrey[5]; const seventhCell = arrey[6]; const eighthCell = arrey[7]; const ninthCell = arrey[8];
    if ((firstCell == 'x' && secondCell == 'x' && thirdCell == 'x') || (fourthCell == 'x' && fifthCell == 'x' && sixthCell == 'x') || (seventhCell == 'x' && eighthCell == 'x' && ninthCell == 'x') || (firstCell == 'x' && fourthCell == 'x' && seventhCell == 'x') || (secondCell == 'x' && fifthCell == 'x' && eighthCell == 'x') || (thirdCell == 'x' && sixthCell == 'x' && ninthCell == 'x') || (firstCell == 'x' && fifthCell == 'x' && ninthCell == 'x') || (thirdCell == 'x' && fifthCell == 'x' && seventhCell == 'x')) {
        winX = true
    } else {
        winX = false
    }
    return winX
}
function winOCondition (arrey) {
    const firstCell = arrey[0]; const secondCell = arrey[1]; const thirdCell = arrey[2]; const fourthCell = arrey[3]; const fifthCell = arrey[4]; const sixthCell = arrey[5]; const seventhCell = arrey[6]; const eighthCell = arrey[7]; const ninthCell = arrey[8];
    if ((firstCell == 'o' && secondCell == 'o' && thirdCell == 'o') || (fourthCell == 'o' && fifthCell == 'o' && sixthCell == 'o') || (seventhCell == 'o' && eighthCell == 'o' && ninthCell == 'o') || (firstCell == 'o' && fourthCell == 'o' && seventhCell == 'o') || (secondCell == 'o' && fifthCell == 'o' && eighthCell == 'o') || (thirdCell == 'o' && sixthCell == 'o' && ninthCell == 'o') || (firstCell == 'o' && fifthCell == 'o' && ninthCell == 'o') || (thirdCell == 'o' && fifthCell == 'o' && seventhCell == 'o')) {
        winO = true
    } else {
        winO = false
    }
    return winO
}
const gameField = new GameField();

// цикл с сомой игорой
for (let c = 0; c < 10; c++) {
    if (gameField.isOverGame() !== false) {
        break
    } else {
        gameField.getGameFieldStatus();
        gameField.fieldCellValue(c);
    }
}

// Вывод сообщения о победе 
gameField.getGameFieldStatus();
if (winXCondition(gameField.state.flat()) == true) {
    alert(`Крестики победили`)
} else if (winOCondition(gameField.state.flat()) == true) {
    alert(`Нолики победили`)
} else {
    alert(`Ничья`)
}
